package com.javasampleapproach.angularjpamysql.repo;


import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.angularjpamysql.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
//	List<Customer> findByLastName(String lastName);
	
	Customer findByUsername(String username);
	
	Customer findByUsernameAndPassword(String username, String password);
	
}