package com.javasampleapproach.angularjpamysql.repo;

import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {
	Iterable<Order> findByFarmer(Farmer farmer);

}
