package com.javasampleapproach.angularjpamysql.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.angularjpamysql.model.Customer;
import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Grade;

public interface GradeRepository  extends CrudRepository<Grade, Integer> {
	Grade findByFarmerAndCustomer(Farmer farmer,Customer customer);
	List<Grade> findByFarmer(Farmer farmer);
}
