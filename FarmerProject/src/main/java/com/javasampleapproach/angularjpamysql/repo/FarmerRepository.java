package com.javasampleapproach.angularjpamysql.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.angularjpamysql.model.Farmer;

public interface FarmerRepository extends CrudRepository<Farmer, Integer>  {
	
	Farmer findByUsernameAndPassword(String username,String password);
	
	List<Farmer> findByAddress(String address);
	
	List<Farmer> findByFarmName(String farmName);
	
}
