package com.javasampleapproach.angularjpamysql.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

	Iterable<Product> findByFarmer(Farmer f);
	Product findByFarmerAndName(Farmer f, String name);
	
	@Transactional
	void deleteById(Integer id);
	
	List<Product> findByName(String name);
}
