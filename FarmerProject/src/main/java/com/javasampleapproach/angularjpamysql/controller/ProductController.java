package com.javasampleapproach.angularjpamysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.message.Response;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/postproduct", method = RequestMethod.POST)
	public void postProduct(@RequestBody Product product) {
		productService.addProduct(product);
	}

	@CrossOrigin(origins = "*") 
	@RequestMapping(value="/updateproduct",method = RequestMethod.POST)
	public void updateProduct(@RequestBody Product product) {
		System.out.println("Im updating");
		productService.update(product);
	}
	@CrossOrigin(origins = "*") 
	@RequestMapping(value="/deleteproduct",method = RequestMethod.POST)
	public void deleteProduct(@RequestBody Product product) {
		productService.delete(product);
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/findbyFarmer/{idFarmer}")
	public Iterable<Product> findByLastName(@PathVariable("idFarmer")Integer idFarmer) {
		Iterable<Product> products= productService.findProducts(idFarmer);
		return products;
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping("/product/{id}")
	public Response findProductById(@PathVariable("id")Integer id) {

		Product product = productService.findProduct(id);

		return new Response("Done", product );
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping("/productName/{name}")
	public List<Product> findProductByName(@PathVariable("name")String name) {
		List<Product> products = productService.findProductByName(name);
		return products;
	}
	
	
	@RequestMapping("/findallproducts")
	public Iterable<Product> findAll() {
		Iterable<Product> products = productService.findProducts();
		return products;
	}
	
	
	
}
