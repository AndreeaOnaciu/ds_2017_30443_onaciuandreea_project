package com.javasampleapproach.angularjpamysql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.message.Response;
import com.javasampleapproach.angularjpamysql.model.Order;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/postorder", method = RequestMethod.POST)
	public void postProduct(@RequestBody Order order) {
		orderService.addOrder(order);
	}

	@CrossOrigin(origins = "*") 
	@RequestMapping(value="/updateorder",method = RequestMethod.POST)
	public void updateProduct(@RequestBody Order order) {
		orderService.updateOrder(order);
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value="/deleteorder",method = RequestMethod.POST)
	public void deleteProduct(@RequestBody Order order) {
		orderService.deleteOrder(order);
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/findOrderByFarmer/{idFarmer}")
	public Iterable<Order> findByLastName(@PathVariable("idFarmer")Integer idFarmer) {
		Iterable<Order> orders= orderService.findOrderByFarmer(idFarmer);
		return orders;
	}
	

}
