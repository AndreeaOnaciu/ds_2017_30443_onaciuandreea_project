package com.javasampleapproach.angularjpamysql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.model.Grade;
import com.javasampleapproach.angularjpamysql.model.Order;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.service.GradeService;

@RestController
public class GradeController {

	@Autowired
	private GradeService gradeService;
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/postgrade", method = RequestMethod.POST)
	public void postCustomer(@RequestBody Grade grade) {
		gradeService.addGrade(grade);
	}
	
	@RequestMapping(value="/updategrade",method = RequestMethod.POST)
	public void updateGrade(@RequestBody Grade grade) {
		gradeService.updateGrade(grade);
	}
	
	@RequestMapping(value="/deletegrade",method = RequestMethod.POST)
	public void deleteGrade(@RequestBody Grade grade) {
		gradeService.deleteGrade(grade);
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/findAverageGrade/{idFarmer}")
	public float findGrade(@PathVariable("idFarmer")Integer idFarmer) {
		 float grade= gradeService.findAverageGrade(idFarmer);
		return grade;
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/findNoPersons/{idFarmer}")
	public int findNoPersons(@PathVariable("idFarmer")Integer idFarmer) {
		 int no= gradeService.findNumberOfPersons(idFarmer);
		return no;
	}
	
}
