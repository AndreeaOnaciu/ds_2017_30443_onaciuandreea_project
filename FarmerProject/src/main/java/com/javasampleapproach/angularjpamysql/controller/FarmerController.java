package com.javasampleapproach.angularjpamysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.message.Response;
import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.service.FarmerService;

@RestController
public class FarmerController {

	@Autowired
	FarmerService farmerService;
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/postfarmer", method = RequestMethod.POST)
	public void postCustomer(@RequestBody Farmer farmer) {
		farmerService.addFarmer(farmer);
	}

	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/loginFarmer", method = RequestMethod.POST)
	public Farmer loginCustomer(@RequestBody Farmer farmer) {
		Farmer f=farmerService.findFarmer(farmer.getUsername(), farmer.getPassword());
		return f;
	}
	
	@RequestMapping("/findallfarmers")
	public Response findAll() {

		Iterable<Farmer> farmers = farmerService.getAll();

		return new Response("Done", farmers);
	}

	@RequestMapping("/farmer/{id}")
	public Response findFarmerById(@PathVariable("id")Integer id) {

		Farmer farmer = farmerService.findFarmer(id);

		return new Response("Done", farmer );
	}
	
	@RequestMapping("/farmer/{username}/{password}")
	public Response findCustomerByUsernameAndPassword(@PathVariable("username") String username,@PathVariable("password") String password) {

		Farmer farmer= farmerService.findFarmer(username, password);

		return new Response("Done", farmer);
	}

	@RequestMapping("/findbyAddressAndFarmName/{address}/{farmName}")
	public Response findByLastName(@PathVariable("address") String address,@PathVariable("farmName") String farmName) {

		List<Farmer> farmers= farmerService.searchFarmer(address, farmName);

		return new Response("Done", farmers);
	}
	
	
	
	
}
