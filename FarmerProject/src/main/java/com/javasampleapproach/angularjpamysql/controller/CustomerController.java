package com.javasampleapproach.angularjpamysql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.message.Response;
import com.javasampleapproach.angularjpamysql.model.Customer;
import com.javasampleapproach.angularjpamysql.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value = "/postcustomer", method = RequestMethod.POST)
	public void postCustomer(@RequestBody Customer customer) {
		customerService.addCustomer(customer);
	}
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/loginCustomer", method = RequestMethod.POST)
	public Customer loginCustomer(@RequestBody Customer customer) {
		return customerService.findCustomer(customer.getUsername(),customer.getPassword());
	}

	@RequestMapping("/findall")
	public Response findAll() {

		Iterable<Customer> customers = customerService.findAll();

		return new Response("Done", customers);
	}

	@RequestMapping("/customer/{username}")
	public Response findCustomerById(@PathVariable("username") String username) {

		Customer customer = customerService.findByUsername(username);

		return new Response("Done", customer);
	}
	
	@RequestMapping("/customer/{username}/{password}")
	public Response findCustomerByUsernameAndPassword(@PathVariable("username") String username,@PathVariable("password") String password) {

		Customer customer = customerService.findCustomer(username, password);

		return new Response("Done", customer);
	}

}
