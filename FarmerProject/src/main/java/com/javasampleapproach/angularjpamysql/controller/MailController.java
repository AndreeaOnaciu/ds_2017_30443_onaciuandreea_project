package com.javasampleapproach.angularjpamysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.angularjpamysql.message.Response;
import com.javasampleapproach.angularjpamysql.model.Mail;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.service.MailService;

@RestController
public class MailController {
	

	@CrossOrigin(origins = "*") 
	@RequestMapping(value = "/postmail", method = RequestMethod.POST)
	public Response postProduct(@RequestBody Mail mail) {
		MailService mailService=new MailService(mail.getUsername(),mail.getPassword());
		return new Response("done",mailService.sendMail(mail.getTo(),mail.getSubject() , mail.getMessage()));	
	}

	
	
}
