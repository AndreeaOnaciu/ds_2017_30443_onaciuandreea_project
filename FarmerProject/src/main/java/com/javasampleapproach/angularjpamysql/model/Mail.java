package com.javasampleapproach.angularjpamysql.model;

import java.io.Serializable;

public class Mail  implements Serializable {

	String username;
	String password;
	String to;
	String message;
	String subject;
	
	public Mail(){
		
	}
	public Mail(String subject,String username, String password, String to, String message) {
		super();
		this.username = username;
		this.password = password;
		this.to = to;
		this.message = message;
		this.subject = subject;
	}
	
	
	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
