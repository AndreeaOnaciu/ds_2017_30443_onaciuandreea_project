package com.javasampleapproach.angularjpamysql.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.angularjpamysql.model.Customer;
import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Order;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.repo.CustomerRepository;
import com.javasampleapproach.angularjpamysql.repo.FarmerRepository;
import com.javasampleapproach.angularjpamysql.repo.OrderRepository;
import com.javasampleapproach.angularjpamysql.repo.ProductRepository;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepo;
	
	@Autowired
	private FarmerRepository farmerRepo;
	
	@Autowired
	private CustomerRepository customerRepo;
	
	@Autowired
	private ProductRepository productRepo;
	//add order
	public void addOrder(Order order){
		Farmer f=farmerRepo.findOne(order.getFarmer().getId());
		Customer c= customerRepo.findOne(order.getCustomer().getId());
		Product p= (Product)order.getProducts().toArray()[0];
		p=productRepo.findOne(p.getId());
		order.setCustomer(c);
		order.setFarmer(f);
		Set<Product> products=new HashSet<Product>();
		products.add(p);
		System.out.println(p);
		order.setProducts(products);
		order.setStatus("new");
		orderRepo.save(order);
	}
	
	//delete order
	public void deleteOrder(Order order){
		orderRepo.delete(order);
	}
	
	//find orders by farmer id
	public Iterable<Order> findOrderByFarmer(Integer id){
		Farmer farmer=farmerRepo.findOne(id);
		return orderRepo.findByFarmer(farmer);
	}
	
	//update order status
	public void updateOrder(Order o){	
		Order order=orderRepo.findOne(o.getId());
		order.setStatus(o.getStatus());
		orderRepo.save(order);
	}
}
