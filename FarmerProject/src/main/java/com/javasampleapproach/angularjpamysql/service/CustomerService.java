package com.javasampleapproach.angularjpamysql.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.angularjpamysql.model.Customer;
import com.javasampleapproach.angularjpamysql.repo.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public void addCustomer(Customer customer){
		customerRepository.save(customer);
	}

	public Iterable<Customer> findAll(){
		return customerRepository.findAll();
	}
	
	public Customer findByUsername(String username) {
		return customerRepository.findByUsername(username);
	}
	
	public void deleteCustomer(String username) {
		Customer customer=customerRepository.findByUsername(username);
		customerRepository.delete(customer);
	}
	
	public Customer findCustomer(String username,String password) {
		return customerRepository.findByUsernameAndPassword(username, password);
	}
}
