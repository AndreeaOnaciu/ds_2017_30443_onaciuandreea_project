package com.javasampleapproach.angularjpamysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Product;
import com.javasampleapproach.angularjpamysql.repo.FarmerRepository;
import com.javasampleapproach.angularjpamysql.repo.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private FarmerRepository farmerRepo;
	
	public void addProduct(Product product){
		productRepo.save(product);
	}
	
	public void update(Product product){
		System.out.println(product.getId());
		Product oldProduct=productRepo.findOne(product.getId());
		oldProduct.setName(product.getName());
		oldProduct.setPrice(product.getPrice());
		oldProduct.setDescription(product.getDescription());
		productRepo.save(oldProduct);
	}
	
	public void delete(Product product){
		System.out.println(product.getId());
		productRepo.deleteById(product.getId());
	}
	
	public Iterable<Product> findProducts(Integer farmerId){
		Farmer farmer=farmerRepo.findOne(farmerId);
		return productRepo.findByFarmer(farmer);
	}
	
	public Iterable<Product> findProducts(){
		return productRepo.findAll();
	}
	
	public Product findProduct(Integer id){
		return productRepo.findOne(id);
	}
	
	public List<Product> findProductByName(String name){
		return productRepo.findByName(name);
	}

}
