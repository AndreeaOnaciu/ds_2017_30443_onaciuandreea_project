package com.javasampleapproach.angularjpamysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.angularjpamysql.model.Customer;
import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.model.Grade;
import com.javasampleapproach.angularjpamysql.repo.FarmerRepository;
import com.javasampleapproach.angularjpamysql.repo.GradeRepository;

@Service
public class GradeService {
	
	@Autowired
	private GradeRepository gradeRepo;
	
	@Autowired
	private FarmerRepository farmerRepo;
	
	public void addGrade(Grade g){
		Grade oldGrade= gradeRepo.findByFarmerAndCustomer(g.getFarmer(),g.getCustomer());
		if (oldGrade==null)
		{
		  gradeRepo.save(g);
		}
		else
		{
		  oldGrade.setGrade(g.getGrade());
		  gradeRepo.save(oldGrade);
		}
	}
	
	public void deleteGrade(Grade g){
		gradeRepo.delete(g);
	}
	
	public void updateGrade(Grade g){
		Grade oldGrade=gradeRepo.findOne(g.getId());
		oldGrade.setGrade(g.getGrade());
		gradeRepo.save(oldGrade);
	}
	
	public  Grade findGrade(Farmer f,Customer c){
		return gradeRepo.findByFarmerAndCustomer(f, c);
	}
	public int findNumberOfPersons(int farmerId) {
		Farmer f=farmerRepo.findOne(farmerId);
		List<Grade> grades=gradeRepo.findByFarmer(f);
		return grades.size();
	}
	public float findAverageGrade(int farmerId) {
		Farmer f=farmerRepo.findOne(farmerId);
		List<Grade> grades=gradeRepo.findByFarmer(f);
		float avg=0f;
		for (Grade g:grades){
			avg+=g.getGrade();
		}
	
		avg=avg/grades.size();
		return avg;
	}
}
