package com.javasampleapproach.angularjpamysql.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.angularjpamysql.model.Farmer;
import com.javasampleapproach.angularjpamysql.repo.FarmerRepository;

@Service
public class FarmerService {
	
	@Autowired
	private FarmerRepository farmerRepository;
	
	public void addFarmer(Farmer f){
		farmerRepository.save(f);
	}
	
 	public Farmer findFarmer(String username,String password) {
		return farmerRepository.findByUsernameAndPassword(username, password);
	}
 	
 	public Farmer findFarmer(Integer id) {
 		return farmerRepository.findOne(id);
 	}

 	public List<Farmer> searchFarmer(String address,String farmName)
 	{
 		List<Farmer> list1=farmerRepository.findByAddress(address);
 		List<Farmer> list2=farmerRepository.findByFarmName(farmName);
 	    Set<Farmer> set = new HashSet<Farmer>();
         
 	    set.addAll(list1);
        set.addAll(list2);
 		
 		return new ArrayList<Farmer>(set);
 	}
 	
 	public Iterable<Farmer> getAll(){
 		return farmerRepository.findAll();
 	}
}
