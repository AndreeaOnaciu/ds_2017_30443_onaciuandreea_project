export interface Farmer {
    id: number;
    address: string;
    description: string;
    email: string;
    farmName: string;
    name: string;
    password: string;
    username: string;
  }