import { Product } from "../farmer-product-page/product";
import { Farmer } from "./Farmer";
import { Customer } from "./Customer";

export interface Order {
    id: number;
    products: Product[];
    status: string;
    farmer: Farmer;
    customer: Customer;
  }