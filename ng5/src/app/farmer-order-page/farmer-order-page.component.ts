import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Order } from './Order';
import { clone } from 'lodash';

@Component({
  selector: 'app-farmer-order-page',
  templateUrl: './farmer-order-page.component.html',
  styleUrls: ['./farmer-order-page.component.scss']
})
export class FarmerOrderPageComponent implements OnInit {

  userId:string="";
  url:string="";
  farmer;
  orders:Order[];
  editedOrder:any = {};
  relatedOrder:any= {};
  email:any={};
  emailText:string='';
  subject:string='';
  myemail:string='';
  password:string='';
  message:string='';

  constructor(private httpClient:HttpClient,private userService:UserService,private router:Router) { }

  ngOnInit() {
    console.log(this.userService.getUser());
    this.userId=this.userService.getUser().id;
    this.url='http://localhost:8080/findOrderByFarmer/'+this.userId;
    console.log("this is the url:");
    console.log( this.url + " end");
    this.farmer=this.userService.getUser();
    this.httpClient.get(this.url).subscribe(
      (data:any[]) => {
      if (data!=null)
      {
        console.log(data);
        this.orders=data;
      }
      else {
        console.log("I m still here");
      }
    });
  }

  productPage(){
    this.router.navigateByUrl("/productPage");
  }

  showEditOrderModal(order) {
    this.editedOrder=clone(order);
  }

  changeStatus() {
    this.httpClient.post('http://localhost:8080/updateorder',
    {
      "id":this.editedOrder.id,
      "farmer":this.editedOrder.farmer,
      "customer":this.editedOrder.customer,
      "status":this.editedOrder.status,
      "products":this.editedOrder.products
      
      }
    ).subscribe(
      (data:any) => {
           
           this.httpClient.get(this.url).subscribe(
            (data:any[]) => {
            if (data!=null)
            { 
              this.orders=data;
            }
            else {
              console.log("I m still here");
            }
          });
        }   
    )
  }

  removeOrder(order: Order) {
    this.httpClient.post('http://localhost:8080/deleteorder',
    {
      "id":order.id,
      "farmer":order.farmer,
      "customer":order.customer,
      "status":order.status,
      "products":order.products
      
      }
    ).subscribe(
      (data:any) => {
                
           this.httpClient.get(this.url).subscribe(
            (data:any[]) => {
            if (data!=null)
            {
              this.orders=data;
            }
            else {
              console.log("I m still here");
            }
          });
        }   
    )
  }

  setRelatedOrder(order:Order){
    this.email=order.customer.email;
    this.relatedOrder=order;
  }

  sendMail(){
    this.httpClient.post('http://localhost:8080/postmail',
    {
      "to":this.email,
      "username":this.myemail,
      "password":this.password,
      "subject":this.subject,
      "message":this.emailText
    }).subscribe(
      (data:any) => {
        this.message=data.data;
        console.log(data);
      }

    );
   
  }
}
