import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerOrderPageComponent } from './farmer-order-page.component';

describe('FarmerOrderPageComponent', () => {
  let component: FarmerOrderPageComponent;
  let fixture: ComponentFixture<FarmerOrderPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmerOrderPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmerOrderPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
