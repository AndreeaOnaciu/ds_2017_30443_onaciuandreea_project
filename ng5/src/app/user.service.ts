import { Injectable } from '@angular/core';
import { Farmer } from './farmer-order-page/Farmer';

@Injectable()
export class UserService {

  private isUserLoggedIn:boolean=false;
  private user;
  private farmer:Farmer;

  constructor() { 
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn() {
    this.isUserLoggedIn = true; 
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  setUser(user){
    console.log("This is the user:");
    console.log(user);
  this.user=user;
    this.isUserLoggedIn=true;
  }

  getUser(){
    return this.user;
  }

  setFarmer(farmer:Farmer) {
    this.farmer=farmer;
  }

  getFarmer() {
    return this.farmer;
  }



}
