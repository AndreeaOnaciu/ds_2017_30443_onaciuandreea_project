import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HttpClientModule } from '@angular/common/http';
import { MatSelectModule, MatButtonModule } from '@angular/material';
import { FarmerProductPageComponent } from './farmer-product-page/farmer-product-page.component';
import { ClientSearchPageComponent } from './client-search-page/client-search-page.component';
import { FarmerOrderPageComponent } from './farmer-order-page/farmer-order-page.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ClientFarmerPageComponent } from './client-farmer-page/client-farmer-page.component';
import { UserService } from './user.service';
import { AuthGuard } from './auth.guard';
//import  { BrowserModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    FarmerProductPageComponent,
    ClientSearchPageComponent,
    FarmerOrderPageComponent,
    ClientFarmerPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFontAwesomeModule
  ],
  providers: [UserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
