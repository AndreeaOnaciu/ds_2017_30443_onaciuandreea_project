import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientSearchPageComponent } from './client-search-page/client-search-page.component';
import { HomeComponent } from './home/home.component';
import { FarmerProductPageComponent} from './farmer-product-page/farmer-product-page.component';
import { FarmerOrderPageComponent } from './farmer-order-page/farmer-order-page.component';
import { ClientFarmerPageComponent } from './client-farmer-page/client-farmer-page.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path:'search',
    canActivate:[AuthGuard],
    component: ClientSearchPageComponent
  },
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'productPage',
    canActivate:[AuthGuard],
    component: FarmerProductPageComponent
  },
  {
    path:'orderPage',
    canActivate:[AuthGuard],
    component: FarmerOrderPageComponent
  },
  {
    path:'farmerPage',
    canActivate:[AuthGuard],
    component: ClientFarmerPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
