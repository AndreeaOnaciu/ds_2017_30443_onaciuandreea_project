import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerProductPageComponent } from './farmer-product-page.component';

describe('FarmerProductPageComponent', () => {
  let component: FarmerProductPageComponent;
  let fixture: ComponentFixture<FarmerProductPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmerProductPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmerProductPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
