import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClient } from '@angular/common/http';
import { Product } from './product';
import { clone } from 'lodash';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-farmer-product-page',
  templateUrl: './farmer-product-page.component.html',
  styleUrls: ['./farmer-product-page.component.scss']
})
export class FarmerProductPageComponent implements OnInit {

  products : Product[];
  productForm: boolean = false;
  editProductForm: boolean = false;
  isNewForm: boolean;
  newProduct: any = {};
  editedProduct: any = {};
  userId;
  url:string;
  farmer:string;
  success:boolean=false;
  message:string="";

  constructor(private httpClient: HttpClient, private router:Router,private userService:UserService) {
   }

  ngOnInit() {
    this.userId=this.userService.getUser().id;
    this.url='http://localhost:8080/findbyFarmer/'+this.userId;
    console.log("this is the url:");
    console.log( this.url + " end");
    this.farmer=this.userService.getUser();
    this.httpClient.get(this.url).subscribe(
      (data:any[]) => {
      if (data!=null)
      {
         this.products=data;
      }
      else {
        console.log("I m still here");
      }
    });
  }

  showEditProductForm(product: Product) {
    if(!product) {
      this.productForm = false;
      return;
    }
    this.editProductForm = true;
    this.editedProduct = clone(product);
  }

  showAddProductForm() {
    // resets form if edited product
    if(this.products.length) {
      this.newProduct = {};
    }
    this.productForm = true;
    this.isNewForm = true;
  }

  saveProduct(product: Product) {
      
      console.log(product);
      this.httpClient.post('http://localhost:8080/postproduct',
      {
        "farmer":this.farmer,
        "price":product.price,
        "description":product.description,
        "name":product.name
        
        }
      ).subscribe(
        (data:any) => {
             this.success=true;     
             this.httpClient.get(this.url).subscribe(
              (data:any[]) => {
              if (data!=null)
              {
                this.products=data;
              }
              else {
                console.log("I m still here");
              }
            });
          }   
      )
  }

  removeProduct(product: Product) {
    console.log(product);
    this.httpClient.post('http://localhost:8080/deleteproduct',
    {
      "id":product.id,
      "farmer":this.farmer,
      "price":product.price,
      "description":product.description,
      "name":product.name
      
      }
    ).subscribe(
      (data:any) => {
           this.success=true;     
           this.httpClient.get(this.url).subscribe(
            (data:any[]) => {
            if (data!=null)
            {

              this.products=data;
            }
            else {
              console.log("I m still here");
            }
          });
        }   
    )
  }

  updateProduct() {
    console.log("In editing method");
    console.log(this.editedProduct.id);
   // this._productService.updateProduct(this.editedProduct);
   this.httpClient.post('http://localhost:8080/updateproduct',
   {
     "id":this.editedProduct.id,
     "farmer":this.farmer,
     "price":this.editedProduct.price,
     "description":this.editedProduct.description,
     "name":this.editedProduct.name
     
     }
   ).subscribe(
     (data:any) => {
          this.success=true;     
          this.httpClient.get(this.url).subscribe(
           (data:any[]) => {
           if (data!=null)
           {
             this.products=data;
           }
           else {
             console.log("I m still here");
           }
         });
       }   
   )

    this.editProductForm = false;
    this.editedProduct = {};
  }

  ordersPage()
  {
    this.router.navigateByUrl("/orderPage");
  }

}
