import { Farmer } from "../farmer-order-page/Farmer";

export interface Product {
    id: number;
    name: string;
    description: string;
    price: number;
    farmer: Farmer;
  }