import { Component, OnInit } from '@angular/core';
import {trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import {FormsModule} from '@angular/forms';
import {HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  username: string = '';
  password: string = '';
  message: string = '';
  andrei:string ='andrei este frumos';
  role: string ='farmer';
  constructor(private httpClient: HttpClient, private router:Router, private userService: UserService) { }

  ngOnInit() {
  }

  login(){
   
    if (this.role==='farmer') {

    this.httpClient.post('http://localhost:8080/loginFarmer',
    {
      "username":this.username,
      "password":this.password,
      "email":"",
      "farmName":"",
      "name":"",
      "description":"",
      "address":""
      }
    ).subscribe(
      (data:any) => {
        if (data==null)
        {
          this.message="Invalid username and password !";
        }
        else {
          this.message="Login successfully !";
          this.userService.setUser(data);
          this.userService.setUserLoggedIn();
          console.log(this.userService.getUserLoggedIn());
          this.router.navigateByUrl('/productPage');
          
        }
      }

    )
  }
  else if (this.role==='customer') {
    this.httpClient.post('http://localhost:8080/loginCustomer',
    {
      "username":this.username,
      "password":this.password,
    }
    ).subscribe(
      (data:any) => {
        if (data==null)
        {
          this.message="Invalid username and password !";
        }
        else {
          this.userService.setUser(data);
          this.userService.setUserLoggedIn();
        
          this.message="Login successfully !";
          this.router.navigateByUrl('/search');
        }
      }

    )
  }
  }

}
