import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFarmerPageComponent } from './client-farmer-page.component';

describe('ClientFarmerPageComponent', () => {
  let component: ClientFarmerPageComponent;
  let fixture: ComponentFixture<ClientFarmerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFarmerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFarmerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
