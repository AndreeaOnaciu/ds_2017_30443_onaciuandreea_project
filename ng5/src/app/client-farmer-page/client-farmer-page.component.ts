import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { Farmer } from '../farmer-order-page/Farmer';
import { Product } from '../farmer-product-page/product';

@Component({
  selector: 'app-client-farmer-page',
  templateUrl: './client-farmer-page.component.html',
  styleUrls: ['./client-farmer-page.component.scss']
})
export class ClientFarmerPageComponent implements OnInit {

  farmer:Farmer;
  url:string='';
  gradeUrl:string='';
  noPersonsUrl:string='';
  products:Product[];
  buyProduct:any={};
  customer:any={};
  grade:any={};
  submitgrade:number=0.0;
  noPersons:any={};
  message:string='';

  constructor(private httpClient:HttpClient, private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.farmer=this.userService.getFarmer();
    this.url='http://localhost:8080/findbyFarmer/'+this.farmer.id;
    this.gradeUrl='http://localhost:8080/findAverageGrade/'+this.farmer.id;
    this.noPersonsUrl='http://localhost:8080/findNoPersons/'+this.farmer.id;
    this.userService.setUserLoggedIn();
    this.httpClient.get(this.url).subscribe(
      (data:any[]) => {
      if (data!=null)
      {
         this.products=data;
      }
      else {
        console.log("I m still here");
      }
    });


    this.httpClient.get(this.gradeUrl).subscribe(
      (data:any[]) => {
      if (data!=null)
      {
         this.grade=data;
         console.log(data);
      }
      else {
        console.log("I m still here");
      }
    });

    this.httpClient.get(this.noPersonsUrl).subscribe(
      (data:any[]) => {
      if (data!=null)
      {
         this.noPersons=data;
         console.log(data);
      }
      else {
        console.log("I m still here");
      }
    });



  }

  setOrderProduct(product: Product)
  {
    this.buyProduct=product;
  }

  orderProduct(product: Product)
  {
      
      this.customer=this.userService.getUser();
      console.log("customer"+this.customer.username);
      console.log("product"+product.name);
      console.log("farmer"+this.farmer.username);
      this.httpClient.post('http://localhost:8080/postorder',
      {
        "farmer":
        {
          "id": this.farmer.id,
          "username": this.farmer.username,
          "password": this.farmer.password,
          "email": this.farmer.email,
          "farmName": this.farmer.farmName,
          "name": this.farmer.name,
          "address": this.farmer.address,
          "description": this.farmer.description
        },
        "customer":{
          "id": this.customer.id,
          "username": this.customer.username,
          "password": this.customer.password,
          "email": this.customer.email
      },
        "products":[{
           "id":product.id,
           "farmer":{
            "id": product.farmer.id,
            "username": product.farmer.username,
            "password": product.farmer.password,
            "email": product.farmer.email,
            "farmName": product.farmer.farmName,
            "name": product.farmer.name,
            "address": product.farmer.address,
            "description": product.farmer.description
           },
           "name":product.name,
           "price":product.price
        }]
      }
      ).subscribe(
        (data:any) => {
          console.log("Order made");
          this.message="Your order has been made successfully!";
        }
      );
  }


  giveGrade(){
    console.log("submitted grade "+this.submitgrade);
    this.customer=this.userService.getUser();
    this.httpClient.post('http://localhost:8080/postgrade',
    {
      "farmer":
      {
        "id": this.farmer.id,
        "username": this.farmer.username,
        "password": this.farmer.password,
        "email": this.farmer.email,
        "farmName": this.farmer.farmName,
        "name": this.farmer.name,
        "address": this.farmer.address,
        "description": this.farmer.description
      },
      "customer":{
        "id": this.customer.id,
        "username": this.customer.username,
        "password": this.customer.password,
        "email": this.customer.email
      },
      "grade":this.submitgrade
    }).subscribe(
      (data:any) => {
        console.log("Grade submitted");
        this.httpClient.get(this.gradeUrl).subscribe(
          (data:any[]) => {
          if (data!=null)
          {
             this.grade=data;
             console.log(data);
          }
          else {
            console.log("I m still here");
          }
        });
        
        this.httpClient.get(this.noPersonsUrl).subscribe(
          (data:any[]) => {
          if (data!=null)
          {
             this.noPersons=data;
             console.log(data);
          }
          else {
            console.log("I m still here");
          }
        });
    

      }
    )

  }
  toProductPage(){
    this.userService.setUserLoggedIn();
    this.router.navigate(['search']);
  }

}
