import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { Product } from '../farmer-product-page/product';



@Component({
  selector: 'app-client-search-page',
  templateUrl: './client-search-page.component.html',
  styleUrls: ['./client-search-page.component.scss']
})
export class ClientSearchPageComponent implements OnInit {

  products : Product[];
  productName: string ='';

  constructor(private httpClient:HttpClient, private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.userService.setUserLoggedIn();
    this.httpClient.get('http://localhost:8080/findallproducts').subscribe(
      (data:any[]) => {
      if (data!=null)
      {
        this.products=data;
      }
      else {
        console.log("No data available");
      }
    });
  }

  searchProduct(){
   this.httpClient.get('http://localhost:8080/productName/'+this.productName).subscribe(
     (data:any[])=> {
      if (data!=null)
      {
        this.products=data;
      }
      else {
        console.log("No data available");
      }
     }
   ) 
  }

  seeFarmer(product:Product){
    this.userService.setFarmer(product.farmer);
    this.userService.setUserLoggedIn();
    this.router.navigate(['farmerPage']);
  }

}
